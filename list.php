<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css">


    <style>
        form {
            padding: 30px 60px 0px 60px;
        }

        .search-item {
            margin-bottom: 20px;
        }

        .search-item label {
            width: 80px;
            margin: 0px;
        }

        select,
        .input_search {
            height: 28px;
            width: 200px;
            border: 1px solid #2E75B6;
            background: #E1EAF4;
        }

        .search_btn,
        .add,
        .edit,
        .delete {
            border: 1px solid #385D8A;
            background: #4F81BD;
            padding: 3px 10px;
            border-radius: 5px;
            color: white;
        }

        .delete, .edit{
            background: #92B1D6;
        }

        .search_btn {
            margin-left: 135px;
        }

        table, th, td{
            border: 0px !important;
            font-weight: normal;
        }
    </style>
</head>

<body>


    <div class="container">
        <form method="post" action="">
            <div class="search-item">
                <label>Khoa</label>
                <select name="khoa" id="khoa">
                    <?php
                    $falcuty = array("NULL" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                    foreach ($falcuty as $key => $value) {
                        echo '<option value="' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="search-item">
                <label>Từ khóa</label>
                <input type="text" class="input_search">
            </div>
            <button class="search_btn mb-3">Tìm kiếm</button>
            <p class="mb-0">Số sinh viên tìm thấy: XXX</p>
        </form>

        <a  href="day07.php"><button class="add float-right mb-4 mr-4"  type="submit" name="add_student">Thêm</button></a>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Tên sinh viên</th>
                    <th scope="col">Khoa</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td class="w-25">Nguyễn Văn A</td>
                    <td class="w-50">Khoa học máy tính</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td class="w-25">Trần Thị B</td>
                    <td class="w-50">Khoa học máy tính</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td class="w-25">Nguyễn Hoàng C</td>
                    <td class="w-50">Khoa học vật liệu</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td class="w-25">Đinh Quang D</td>
                    <td class="w-50">Khoa học vật liệu</td>
                    <td class="w-10">
                        <button class="delete">Xóa</button>
                        <button class="edit">Sửa</button>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</body>

</html>